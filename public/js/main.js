"use strict"

//HEADER SECTION
function addSmoothScroll(event) {
    event.preventDefault();
    const blockID = this.getAttribute("href");
    document.querySelector(blockID).scrollIntoView({
        behavior: "smooth",
        block: "start"
    })
}

const anchors = document.querySelectorAll("a.scroll-to");

for (let anchor of anchors) {
    anchor.addEventListener("click", addSmoothScroll);
}


//SERVICES SECTION
function activeServicesTab() {
    servicesTabs.forEach((tab) => {
        if (tab.classList.contains("services-tabs__item__active")) tab.classList.remove("services-tabs__item__active");
    });
    this.classList.add("services-tabs__item__active");
    servicesTabsContent.forEach(tabsContentItem => {
        if (tabsContentItem.dataset.contentName === this.textContent) {
            tabsContentItem.style.display = "flex";
        } else tabsContentItem.style.display = "none";
    })
}

const servicesTabs = document.querySelectorAll(".services-tabs__item");
const servicesTabsContent = document.querySelectorAll(".services-content__item");

servicesTabs.forEach((tabsItem) => {
    tabsItem.addEventListener("click", activeServicesTab);
});


//OUR AMAZING WORK SECTION
function filterAmazingTab() {
    amazingTabs.forEach((tab) => {
        if (tab.classList.contains("amazing-work-tabs__item__active")) tab.classList.remove("amazing-work-tabs__item__active");
    });
    this.classList.add("amazing-work-tabs__item__active");
    amazingGridItem.forEach((amazingPictureDiv, divIndex) => {
        const amazingPicture = amazingPictureDiv.firstElementChild;
        const visibleDivAmount = (amazingPressingCounter + 1) * 12;
        if (this.textContent === "All" && divIndex < visibleDivAmount) {
            amazingPictureDiv.style.display = "block";
            return;
        }
        amazingPictureDiv.style.display = "none";
        if ("dataset" in amazingPicture) {
            if (amazingPicture.dataset.category === this.textContent && divIndex < visibleDivAmount) {
                amazingPictureDiv.style.display = "block";
            }
        }
    })
}

function loadAmazingPictures() {
    amazingLoaderAnimation.style.display = "none";
    const currentTabName = document.querySelector(".amazing-work-tabs__item__active").textContent;
    const visibleDivAmount = amazingPressingCounter * 12;
    for (let i = visibleDivAmount; i < (visibleDivAmount + 12); i++) {
        const currentImg = amazingPictureGrid.children[i].firstElementChild;
        if (currentImg.dataset.category === currentTabName || currentTabName === "All") {
            amazingPictureGrid.children[i].style.display = "block";
        }
    }
    if (amazingPressingCounter === 1) amazingLoaderButton.style.display = "inline-block";
}

const amazingTabs = document.querySelectorAll(".amazing-work-tabs__item");
const amazingGridItem = document.querySelectorAll(".amazing-work-picture-grid > div");
let amazingPressingCounter = 0;

amazingTabs.forEach((amazingTabsItem) => {
    amazingTabsItem.addEventListener("click", filterAmazingTab);
});

const amazingLoaderButton = document.querySelector(".amazing-work-button");
const amazingLoaderAnimation = document.querySelector(".amazing-work-loader-animation");
const amazingPictureGrid = document.querySelector(".amazing-work-picture-grid");

amazingLoaderButton.addEventListener("click", (event) => event.preventDefault());
amazingLoaderButton.addEventListener("click", () => {
    amazingPressingCounter++;
    amazingLoaderButton.style.display = "none";
    amazingLoaderAnimation.style.display = "flex";
});
amazingLoaderAnimation.addEventListener("animationend", loadAmazingPictures);


//GALLERY SECTION
function galleryLoadMore() {
    galleryButton.style.display = "none";
    galleryLoaderAnimation.style.display = "flex";
    galleryLoaderAnimation.addEventListener("animationend", () => {
        galleryLoaderAnimation.style.display = "none";
        const gridItems = document.querySelectorAll(".gallery-grid-item");
        gridItems.forEach((item, index) => {
            if (index >= 9) item.style.display = "";
        });
        msnry.layout();
        galleryGrid.scrollIntoView(false);
    })
}

const galleryGrid = document.querySelector(".gallery-grid");
const msnry = new Masonry(galleryGrid, {
    itemSelector: ".gallery-grid-item",
    horizontalOrder: true,
    gutter: 20,
    fitWidth: true
});

const galleryButton = document.querySelector(".gallery-button");
const galleryLoaderAnimation = document.querySelector(".gallery-loader-animation");
galleryButton.addEventListener("click", (event) => event.preventDefault());
galleryButton.addEventListener("click", galleryLoadMore);