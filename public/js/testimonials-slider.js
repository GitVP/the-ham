//TESTIMONIALS SECTION
function moveOnePositionTest(event) {
    let newSelectedItem;
    let moveValue;
    const currentSelectedItem = document.querySelector(".testimonials-photo-list__item__active");
    if (event.target.dataset.direction === "left") {
        newSelectedItem = currentSelectedItem.previousElementSibling;
        moveValue = 100;
    } else {
        newSelectedItem = currentSelectedItem.nextElementSibling;
        moveValue = -100;
    }
    if (newSelectedItem !== null) {
        currentSelectedItem.className = "testimonials-photo-list__item";
        newSelectedItem.className = "testimonials-photo-list__item__active";
        document.querySelectorAll(".testimonials-data-list__item").forEach(item => item.style.visibility = "visible");
        const dataList = document.querySelector(".testimonials-data-list");
        dataList.style.left = `${parseInt(dataList.style.left) + moveValue}%`;
    } else {
        newSelectedItem = event.target.dataset.direction === "left" ? document.querySelector(".testimonials-photo-list__item:last-child") : document.querySelector(".testimonials-photo-list__item:first-child");
        currentSelectedItem.className = "testimonials-photo-list__item";
        newSelectedItem.className = "testimonials-photo-list__item__active";
        moveToSelectedItemTest(currentSelectedItem, newSelectedItem);
    }
}

function selectSmallPhotoTest(event) {
    if ("tagName" in event.target && event.target.tagName === "IMG") {
        const currentSelectedItem = document.querySelector(".testimonials-photo-list__item__active");
        const newSelectedItem = event.target.parentElement;
        currentSelectedItem.className = "testimonials-photo-list__item";
        newSelectedItem.className = "testimonials-photo-list__item__active";
        moveToSelectedItemTest(currentSelectedItem, newSelectedItem);
    }
}

function moveToSelectedItemTest(currentSelectedItem, newSelectedItem) {
    const dataList = document.querySelector(".testimonials-data-list");
    const curPos = Array.from(dataList.children).findIndex((item) => item.dataset.author === currentSelectedItem.dataset.author);
    const newPos = Array.from(dataList.children).findIndex((item) => item.dataset.author === newSelectedItem.dataset.author);
    const dataListItems = document.querySelectorAll(".testimonials-data-list__item");
    dataListItems.forEach((item, index) => {
        item.style.visibility = "visible";
        const from = (curPos > newPos) ? newPos : curPos;
        const to = (curPos > newPos) ? curPos : newPos;
        if (index > from && index < to) {
            item.style.visibility = "hidden";
        }
    });
    dataList.style.left = `${newPos * -100}%`;
}

const testArrowBtns = document.querySelectorAll(".testimonials-arrow-button");
testArrowBtns.forEach((arrowButton) => {
    arrowButton.addEventListener("click", (event) => event.preventDefault());
    arrowButton.addEventListener("click", moveOnePositionTest);
});
const testPhotoList = document.querySelector(".testimonials-photo-list");
testPhotoList.addEventListener("click", selectSmallPhotoTest);